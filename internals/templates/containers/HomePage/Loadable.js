/**
 * Asynchronously loads the component for ReferPage
 */

import loadable from 'utils/loadable';

export default loadable(() => import('./index'));
