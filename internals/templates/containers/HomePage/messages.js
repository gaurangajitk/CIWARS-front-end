/*
 * ReferPage Messages
 *
 * This contains all the text for the ReferPage container.
 */
import { defineMessages } from 'react-intl';

export const scope = 'app.containers.ReferPage';

export default defineMessages({
  header: {
    id: `${scope}.header`,
    defaultMessage: 'This is the ReferPage container!',
  },
});
