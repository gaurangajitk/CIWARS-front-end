import React from 'react';
import { render } from 'react-testing-library';
import { IntlProvider } from 'react-intl';

import ReferPage from '../index';

describe('<ReferPage />', () => {
  it('should render and match the snapshot', () => {
    const {
      container: { firstChild },
    } = render(
      <IntlProvider locale="en">
        <ReferPage />
      </IntlProvider>,
    );
    expect(firstChild).toMatchSnapshot();
  });
});
