import React from 'react';
import ReactDOM from 'react-dom';
import DataTable from 'react-data-table-component';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
//import data from './data.json';

//import './SubmitMultipleSamplesPage.css';

const uploadRoute = 'https://petstore.swagger.io/v2/pet';
const uploadedFiles = [];
//const selectedFiles = [];
//var cnt = 0;

let filename_content_map = new Map();

const columns = [
    {
      name: 'SampleName',
      selector: 'samplename',
      sortable: true,
    },
    {
      name: 'Type',
      selector: 'type',
      sortable: true,
    },
    {
      name: 'Platform',
      selector: 'platform',
      sortable: true,
    },
    {
      name: 'ForwardReadFile',
      selector: 'forward_file_path',
    },
    {
      name: 'ReverseReadFile',
      selector: 'reverse_file_path',
    },
  ];

class UserHomePage extends React.Component {
  
  constructor(props) {
    
    super(props);
    this.state = {
      DATA: null,
    };
  }

  componentDidMount() {
    this.uploadFile();
  }
  
  uploadFile() {
    // POST request using fetch with error handling
    
    const requestOptions = {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({
        userid:'davidyao',
      }),
    };
    /*const requestOptions = {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({
        component: 'MetaCompare',
        sampleName: this.state.rows[i]["sampleName"],
        type: this.state.rows[i]["type"],
        input: {
          fileF: {
            fileName: this.state.rows[i]["fileF"],
            base64Data: this.state.rows[i]["base64DataF"],
          },
          fileR: {
            fileName: this.state.rows[i]["fileR"],
            base64Data: this.state.rows[i]["base64DataR"],
          },
        },
      }),
    };*/
    //requests.push(requestOptions);
    console.log(requestOptions);
    
    fetch(uploadRoute, requestOptions)
      .then(async response => {
        const data = await response.json();
        console.log(response.status);
        // check for error response
        if (!response.ok) {
          // get error message from body or default to response status
          const error = (data && data.message) || response.status;
          
          //this.props.history.push("/showError");
          
          return Promise.reject(error);
        }
        this.setState({ DATA: data });
        console.log('Success');
        console.log(response);
        this.setState({ response });
      })
      .catch(error => {
        console.log("error");
        console.error('There was an error!', error);
      });
  }
  
  render() {
    
    return (
        <div>
        <DataTable
          title="Uploaded Samples"
          columns={columns}
          data={this.state.DATA}
          highlightOnHover
          pagination
          paginationPerPage={5}
          paginationRowsPerPageOptions={[5, 15, 25, 50]}
          paginationComponentOptions={{
            rowsPerPageText: 'Records per page:',
            rangeSeparatorText: 'out of',
          }}
        />
      </div>
    );
  }
}

export default UserHomePage;
