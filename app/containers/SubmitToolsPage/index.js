/* eslint-disable no-console */
import React from 'react';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import DropdownButton from 'react-bootstrap/DropdownButton'

import './SubmitToolsPage.css';

const uploadRoute = 'https://petstore.swagger.io/v2/pet';
var selectedTools = new Map();
var cnt = 0;
var st = true;

class SubmitTools extends React.Component {
  constructor(props) {
    
    super(props);
    this.state = {
      sampleName: '',
      selectedTools: null,
    };
  }
  
  validateUpload = () => selectedTools.size == 5;
  //this.state.selectedFiles !== null;

  handleSubmit = () => {
    console.log(this.state.sampleName, this.state.selectedTools);
    this.uploadFile();
  };
  
  
  onChangeHandler = event => {
    const toolname = event.target.value;
    const tool = event.target.id;
    console.log(toolname);
    if (toolname!=""){
      if (selectedTools.has(tool)){
        selectedTools.set(tool, {name: toolname});
      }
      else{
        selectedTools.delete(tool);
        selectedTools.set(tool, {name: toolname});
      }
      //cnt++;
      this.setState({ selectedTools });
    }
    else{
      selectedTools.delete(tool);
      //cnt = Object.keys(selectedTools).length;
    }
    console.log(selectedTools.size);
    if (selectedTools.size === 5){
      st = false;
    }
    else{
      st = true;
    }
  };



  /*
  onChangeHandler = event => {
    const toolname = event.target.value;
    const tool = event.target.id;
    selectedTools.push({
      name: toolname,
      tool: tool,
    });
    cnt++;
    this.setState({ selectedTools });
  };*/

  uploadFile() {
    // POST request using fetch with error handling
    const formData = new FormData();
    formData.append('title', this.state.sampleName);
    formData.append('tools', this.state.selectedTools);

    const requestOptions = {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({
        component: 'MetaCompare',
        sampleName: this.state.sampleName,
        input: {
          qc: this.state.selectedTools.get('qc'),
          assembly: this.state.selectedTools.get('a'),
          gene_prediction: this.state.selectedTools.get('gp'),
          annotation: this.state.selectedTools.get('an'),
          taxonomoy_classification: this.state.selectedTools.get('tx'),
        },
      }),
    };
    console.log(requestOptions);

    fetch(uploadRoute, requestOptions)
      .then(async response => {
        const data = await response.json();

        // check for error response
        if (!response.ok) {
          // get error message from body or default to response status
          const error = (data && data.message) || response.status;
          return Promise.reject(error);
        }
        console.log('Your sample has been submitted');
        console.log(response);
        this.setState({ response });
      })
      .catch(error => {
        console.error('There was an error!', error);
      });
  }

  render() {
    return (
      <div className="outer">
        <div className="inner">
          <Form>
            <h3>Select Tools</h3>
            <Form.Group size="lg">
              <p>
                Select the tools to run the Pipeline 
              </p>
            </Form.Group>

            <form>
              <label>
                Pick which <b>quality control</b> tool you want to run: &nbsp;
                  <select id='qc' onChange={this.onChangeHandler}>
                    <option value="" selected disabled>-- choose one -- </option>
                    <option value="fastp">fastp</option>
                    <option value="trimmomatic">trimmomatic</option>
                  </select>
              </label>
            </form>

            <form>
              <label>
                Pick which <b>assembly</b> tool you want to run: &nbsp;
                  <select id='a' onChange={this.onChangeHandler}>
                    <option value="" selected disabled>-- choose one -- </option>
                    <option value="IDBA">IDBA-UD</option>
                    <option value="megahit">MegaHit</option>
                  </select>
              </label>
            </form>

            <form> 
              <label>
                Pick which <b>gene prediction</b> tool you want to run: &nbsp;
                  <select id='gp' onChange={this.onChangeHandler}>
                    <option value="" selected disabled>-- choose one -- </option>
                    <option value="prodigal">prodigal</option>
                  </select>
              </label>
            </form>

            <form>
              <label>
                Pick which <b>annotation</b> tool you want to run: &nbsp;
                  <select id='an' onChange={this.onChangeHandler}>
                    <option value="" selected disabled>-- choose one -- </option>
                    <option value="diamond">DIAMOND</option>
                    <option value="deeparg">DeepARG</option>
                  </select>
              </label>
            </form>

            <form>
              <label>
                Pick which <b>taxonomoy classification</b> tool you want to run: &nbsp;
                  <select id='tx' onChange={this.onChangeHandler}>
                    <option value="" selected disabled>-- choose one -- </option>
                    <option value="kraken2">Kreken2</option>
                    <option value="kaiju">kaiju</option>
                  </select>
              </label>
            </form>



            <Button
              block
              size="lg"
              disabled={st}
              onClick={this.handleSubmit}
            >
              Submit
            </Button>

          </Form>

        </div>
        {this.state.response && (
          <div className="inner">
            {' '}
            <p>Pipeline has started running</p>{' '}
          </div>
        )}
      </div>
    );
  }
}

export default SubmitTools;
