import React from 'react';
import ReactDOM from 'react-dom';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';

import './SubmitMultipleSamplesPage.css';

const uploadRoute = 'https://petstore.swagger.io/v2/pet';
const uploadedFiles = [];
//const selectedFiles = [];
//var cnt = 0;

let filename_content_map = new Map();

class SubmitMultipleSamples extends React.Component {
  state = {
    rows: [],
    uploadedFiles: null,
  };

  handleChangeFiles = event => {
    this.RemoveAllElementsRow();
    uploadedFiles.length = 0;
    const { files } = event.target;
    //const rows = [...this.state.rows];
    var output = document.getElementById('fileList');
    var children = "";
    for(let i = 0; i < files.length; i++){
      const reader = new FileReader();
      reader.readAsDataURL(files[i]);
      reader.onload = e => {
        /*rows[idx] = {
          fileF: files[0].name,
          base64DataF: reader.result,
        };*/
        //rows[idx]["fileF"] = files[0].name;
        //rows[idx]["base64DataF"] = reader.result;
        filename_content_map.set(files[i].name, reader.result);
        uploadedFiles.push(files[i].name);
      };
      children += '<li>' + files[i].name + '</li>';
    }
    output.innerHTML = '<ul>'+children+'</ul>';
    this.setState({uploadedFiles});
    /*
    this.setState({
      rows
    });*/
  };
  validateAddSample = () => this.state.uploadedFiles !==null;

  handleChange = idx => e => {
    const { name, value } = e.target;
    const rows = [...this.state.rows];
    /*
    rows[idx] = {
      sampleName: value
    };*/

    rows[idx]["sampleName"] = value;
    this.setState({
      rows
    });
  };

  handleChangetype = idx => e => {
    const { name, value } = e.target;
    const rows = [...this.state.rows];
    /*
    rows[idx] = {
      sampleName: value
    };*/

    rows[idx]["type"] = value;
    this.setState({
      rows
    });
  };

  handleChangeplatform = idx => e => {
    const { name, value } = e.target;
    const rows = [...this.state.rows];
    /*
    rows[idx] = {
      sampleName: value
    };*/

    rows[idx]["platform"] = value;
    this.setState({
      rows
    });
  };



  handleAddRow = () => {
    const item = {
      sampleName: "",
      type: "",
      platform: "",

      fileF: "",
      base64DataF: "",

      fileR: "",
      base64DataR: "",

    };
    this.setState({
      rows: [...this.state.rows, item]
    });
  };
  RemoveAllElementsRow(){
    while(this.state.rows.length){
      this.state.rows.pop();
    }
    this.setState({
      rows: this.state.rows,
    });
  }
  handleRemoveRow = () => {
    this.setState({
      rows: this.state.rows.slice(0, -1)
    });
  };
  handleRemoveSpecificRow = (idx) => () => {
    const rows = [...this.state.rows]
    rows.splice(idx, 1)
    this.setState({ rows })
  }

  handleSubmit = () => {
    //console.log(this.state.rows);
    if (this.state.rows.length === 0){
      console.log("invalid submit");
      return;
    }
    var submit_flag = true;
    for(let i = 0; i < this.state.rows.length; i++){
      if (this.state.rows[i]["sampleName"] && this.state.rows[i]["fileF"] && this.state.rows[i]["fileR"] && this.state.rows[i]['type'] && this.state.rows[i]['platform']){
        continue;
      }
      else{
        submit_flag = false;
        break;
      }
    }
    if (submit_flag){
      this.uploadFile();
    }
    else{
      console.log("invalid submit");
    }
  };

  uploadFile() {
    // POST request using fetch with error handling
    uploadedFiles.splice(0, uploadedFiles.length);
    const how_many_samples = this.state.rows.length;
    const requests = [];
    /*requests.push({
      how_many_samples: how_many_samples,
    });*/
    var data = {};
    data['component'] = 'MetaCompare';
    data['numberofsamples'] = how_many_samples;
    for(let i = 0; i< this.state.rows.length; i++){
      let x = i + 1;
      let x_str = x.toString();
      var key1 = 'sampleName'+x_str;
      var key2 = 'fileF'+x_str;
      var key3 = 'base64DataF'+x_str;
      var key4 = 'fileR'+x_str;
      var key5 = 'base64DataR'+x_str;

      var key6 = 'type'+x_str;
      var key7 = 'platform'+x_str;
      data[key1] = this.state.rows[i]["sampleName"];
      data[key2] = this.state.rows[i]["fileF"];
      data[key3] = this.state.rows[i]["base64DataF"];
      data[key4] = this.state.rows[i]["fileR"];
      data[key5] = this.state.rows[i]["base64DataR"];
      data[key6] = this.state.rows[i]['type'];
      data[key7] = this.state.rows[i]['platform'];
    }
    const requestOptions = {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify(data),
    };
    /*const requestOptions = {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({
        component: 'MetaCompare',
        sampleName: this.state.rows[i]["sampleName"],
        type: this.state.rows[i]["type"],
        input: {
          fileF: {
            fileName: this.state.rows[i]["fileF"],
            base64Data: this.state.rows[i]["base64DataF"],
          },
          fileR: {
            fileName: this.state.rows[i]["fileR"],
            base64Data: this.state.rows[i]["base64DataR"],
          },
        },
      }),
    };*/
    //requests.push(requestOptions);
    console.log(requestOptions);
    
    fetch(uploadRoute, requestOptions)
      .then(async response => {
        const data = await response.json();
        console.log(response.status);
        // check for error response
        if (!response.ok) {
          // get error message from body or default to response status
          const error = (data && data.message) || response.status;
          
          //this.props.history.push("/showError");
          
          return Promise.reject(error);
        }
        console.log('Your sample has been submitted');
        console.log(response);
        this.setState({ response });
      })
      .catch(error => {
        console.log("error");
        console.error('There was an error!', error);
      });
  }
  createSelectItems() {
    let items = []; 
    items.push(<option value="" selected disabled>-- choose one -- </option>);        
    for (let i = 0; i <uploadedFiles.length; i++) {             
         items.push(<option key={uploadedFiles[i]} value={uploadedFiles[i]}>{uploadedFiles[i]}</option>);   
         //here I will be creating my options dynamically based on
         //what props are currently passed to the parent component
    }
    //console.log(items);
    return items;
  }  

  onDropdownSelectedFrd= idx => e => {
    console.log("THE VAL", e.target.value);
    const rows = [...this.state.rows];
    rows[idx]["fileF"] = e.target.value;
    rows[idx]["base64DataF"] = filename_content_map.get(e.target.value);

    this.setState({
      rows: this.state.rows,
    });
   //here you will see the current selected value of the select input
  }

  onDropdownSelectedRev= idx => e => {
    console.log("THE VAL", e.target.value);
    const rows = [...this.state.rows];
    rows[idx]["fileR"] = e.target.value;
    rows[idx]["base64DataR"] = filename_content_map.get(e.target.value);

    this.setState({
      rows: this.state.rows,
    });
   //here you will see the current selected value of the select input
  }
 
  render() {
    
    return (
      <div>
        <div>
          <div>
            <div>
            <h3>Choose Paired-End Read Files</h3>
            <Form.Group>
              <Form.File
                id="FILES"
                multiple
                //single
                onChange={this.handleChangeFiles}
              />
            </Form.Group>
            </div>
            <div id="fileList" className="ulscroll"></div>
            <br/>
            <div className="responsiveTable">
              {uploadedFiles.length>0 &&
                <table
                  id="tab_logic"
                >
                  <thead>
                    <tr>
                      <th>  # </th>
                      <th> Sample Name </th>
                      <th> Type </th>
                      <th> Platform </th>
                      <th> Forward Read File </th>
                      <th>&nbsp;</th>
                      <th> Reverse Read File </th>
                      <th />
                    </tr>
                  </thead>
                  <tbody>
                    {this.state.rows.map((item, idx) => (
                      <tr id="addr0" key={idx}>
                        <td className="removebutton">{idx}</td>
                        <td className="removebutton">
                          
                          <input
                            type="text"
                            onChange={this.handleChange(idx)}
                            placeholder="Enter Sample Name"
                          >
                          </input>
                        </td>
                        <td>
                          <input
                            type="text"
                            onChange={this.handleChangetype(idx)}
                            placeholder="e.g metagenome/genome"
                          >
                          </input>
                        </td>

                        <td>
                          <input
                            type="text"
                            onChange={this.handleChangeplatform(idx)}
                            placeholder="e.g Illumina"
                          >
                          </input>
                        </td>

                        <td>
                          <div>
                            <select onChange={this.onDropdownSelectedFrd(idx)} label="Multiple Select" single size="sm">
                                {this.createSelectItems()}
                            </select>
                          </div>
                        </td>
                        <td>
                          &nbsp;
                        </td>
                        <td>
                          <div>
                            <select onChange={this.onDropdownSelectedRev(idx)} label="Multiple Select" single size="sm">
                                {this.createSelectItems()}
                            </select>
                          </div>
                        </td>
                        <td>
                          <Button
                            //block
                            className="removebutton"
                            size="sm"
                            disabled={false}
                            onClick={this.handleRemoveSpecificRow(idx)}
                          >
                            Remove
                          </Button>
                        </td>
                      </tr>
                    ))}
                  </tbody>
                </table>
              }
              <button onClick={this.handleAddRow} className="btn btn-primary" disabled={!this.validateAddSample()} >
                Add New Sample
              </button>
              &nbsp;
              <button
              disabled={false} 
              onClick={this.handleSubmit} 
              className="btn btn-primary"
              >
                Submit Sample(s)
              </button>
              {this.state.response && (
                <div className="inner">
                  {' '}
                  <p>Your sample has been successfully submitted</p>{' '}
                </div>
              )}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default SubmitMultipleSamples;
