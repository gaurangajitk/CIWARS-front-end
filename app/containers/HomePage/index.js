import React from 'react';
import PropTypes from 'prop-types';
import Button from 'react-bootstrap/Button';

import './HomePage.css';

class HomePage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoggedin: props.isLoggedin,
    };
  }

  render() {
    return (
      <div className="main">
        <div className="image">
          <section> CIWARS </section>
          <div className="getStarted">
            <Button href="/login" block size="lg" type="submit">
              Get Started
            </Button>
          </div>
        </div>
      </div>
    );
  }
}

HomePage.propTypes = {
  isLoggedin: PropTypes.bool,
};

HomePage.defaultProps = {
  isLoggedin: false,
};

export default HomePage;
