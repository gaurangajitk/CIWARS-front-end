/* eslint-disable no-console */
import React from 'react';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';

import './SubmitSamplePage.css';

const uploadRoute = 'https://petstore.swagger.io/v2/pet';
const selectedFiles = [];
var cnt = 0;

class SubmitSample extends React.Component {
  constructor(props) {
    
    super(props);
    this.state = {
      sampleName: '',
      selectedFiles: null,
    };
  }
  
  validateUpload = () => cnt >= 2;
  //this.state.selectedFiles !== null;

  handleSubmit = () => {
    console.log(this.state.sampleName, this.state.selectedFiles);
    this.uploadFile();
  };
  
  onChangeHandler = event => {
    const { files } = event.target;
    
    //for (let i = 0; i <= 0 ; i++) {
      const reader = new FileReader();
      reader.readAsDataURL(files[0]);
      reader.onload = e => {
        selectedFiles.push({
          name: files[0].name,
          base64Data: reader.result,  // e.event.result
        });
      };
      cnt++;
    //}
    this.setState({ selectedFiles });
  };

  uploadFile() {
    // POST request using fetch with error handling
    const formData = new FormData();
    formData.append('title', this.state.sampleName);
    formData.append('file', this.state.selectedFiles);

    const requestOptions = {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({
        component: 'MetaCompare',
        sampleName: this.state.sampleName,
        input: {
          file1: this.state.selectedFiles[0],
          file2: this.state.selectedFiles[1],
        },
      }),
    };
    console.log(requestOptions);

    fetch(uploadRoute, requestOptions)
      .then(async response => {
        const data = await response.json();

        // check for error response
        if (!response.ok) {
          // get error message from body or default to response status
          const error = (data && data.message) || response.status;
          return Promise.reject(error);
        }
        console.log('Your sample has been submitted');
        console.log(response);
        this.setState({ response });
      })
      .catch(error => {
        console.error('There was an error!', error);
      });
  }

  render() {
    return (
      <div className="outer">
        <div className="inner">
          <Form>
            <h3>Upload Sample</h3>
            <Form.Group size="lg">
              <p>
                Upload two FASTA/FASTQ files containing paired-end short read sequences 
              </p>
            </Form.Group>
            <Form.Group size="lg">
              <Form.Label>Sample Name</Form.Label>
              <Form.Control
                autoFocus
                onChange={e => this.setState({ sampleName: e.target.value })}
                placeholder="Enter Sample Name"
              />
            </Form.Group>
            <Form.Group>
              <Form.File
                id="fileF"
                label="Select forward read file"
                //multiple
                single
                onChange={this.onChangeHandler}
              />
            </Form.Group>

            <Form.Group>
              <Form.File
                id="fileR"
                label="Select reverse read file"
                //multiple
                single
                onChange={this.onChangeHandler}
              />
            </Form.Group>

            <Button
              block
              size="lg"
              disabled={!this.validateUpload()}
              onClick={this.handleSubmit}
            >
              Submit
            </Button>
          </Form>
        </div>
        {this.state.response && (
          <div className="inner">
            {' '}
            <p>Your sample has been successfully submitted</p>{' '}
          </div>
        )}
      </div>
    );
  }
}

export default SubmitSample;
