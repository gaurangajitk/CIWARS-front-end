import React from 'react';
import { Redirect } from 'react-router-dom';
function ShowError() {
    return (
        <h3> An error has occurred in the back-end server</h3>
    );
}
export default ShowError;