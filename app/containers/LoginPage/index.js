import React, { useState } from 'react';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import { Link } from 'react-router-dom';

import './LoginPage.css';

export default function Login() {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  function validateForm() {
    return email.length > 0 && password.length > 0;
  }

  function handleSubmit() {
    console.log(email, password);
  }

  return (
    <div className="outer">
      <div className="inner">
        <Form onSubmit={handleSubmit}>
          <h3>Login</h3>
          <Form.Group size="lg">
            <Form.Label>Email</Form.Label>
            <Form.Control
              autoFocus
              type="email"
              value={email}
              onChange={e => setEmail(e.target.value)}
              placeholder="Enter Email"
            />
          </Form.Group>
          <Form.Group size="lg" controlId="password">
            <Form.Label>Password</Form.Label>
            <Form.Control
              type="password"
              value={password}
              onChange={e => setPassword(e.target.value)}
              placeholder="Enter Password"
            />
          </Form.Group>

          <Button block size="lg" disabled={!validateForm()}>
            Login
          </Button>

          <div className="registerMessage">
            <span>Dont have an account? </span>
            <Link to="/register">
              <span>Register</span>
            </Link>
          </div>
        </Form>
      </div>
    </div>
  );
}
