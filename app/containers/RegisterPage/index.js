import React, { useState } from 'react';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import { Link } from 'react-router-dom';

import '../LoginPage/LoginPage.css';

function RegisterPage() {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [confirmPassword, setConfirmPassword] = useState('');

  const validateForm = () =>
    validateEmail(email) && validatePassword(password, confirmPassword);

  const validateEmail = email => {
    const emailPattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
    return emailPattern.test(email);
  };

  const validatePassword = (password, confirmPassword) =>
    password.length > 0 && password === confirmPassword;

  function handleSubmit(event) {
    console.log(email, password, confirmPassword);
  }

  return (
    <div className="outer">
      <div className="inner">
        <Form onSubmit={handleSubmit}>
          <h3>Register</h3>
          <Form.Group size="lg">
            <Form.Label>Email</Form.Label>
            <Form.Control
              autoFocus
              type="email"
              value={email}
              onChange={e => setEmail(e.target.value)}
              placeholder="Enter Email"
            />
          </Form.Group>
          <Form.Group size="lg" controlId="password">
            <Form.Label>Password</Form.Label>
            <Form.Control
              type="password"
              value={password}
              onChange={e => setPassword(e.target.value)}
              placeholder="Enter Password"
            />
          </Form.Group>
          <Form.Group size="lg" controlId="confirmPassword">
            <Form.Label>Confirm Password</Form.Label>
            <Form.Control
              type="password"
              value={confirmPassword}
              onChange={e => setConfirmPassword(e.target.value)}
              placeholder="Confirm Password"
            />
          </Form.Group>
          <Button block size="lg" type="submit" disabled={!validateForm()}>
            Submit
          </Button>
          <div className="registerMessage">
            <span>Already have an account? </span>
            <Link to="/login">
              <span>Sign in</span>
            </Link>
          </div>
        </Form>
      </div>
    </div>
  );
}

export default RegisterPage;
