import React from 'react';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';

import './DashboardPage.css';

function DashboardPage() {
  return (
    <div className="parent">
      <div className="row">
        <div className="rowElement">
          <Form onSubmit={() => {}}>
            <Form.Group className="formTitle">
              <h3>Create Project</h3>
            </Form.Group>
            <Form.Group size="lg">
              <Form.Label>Name</Form.Label>
              <Form.Control
                type="email"
                onChange={() => {}}
                placeholder="Enter Project Name"
              />
            </Form.Group>
            <Form.Group>
              <Form.Label>Project Description</Form.Label>
              <Form.Control as="textarea" rows={5} />
            </Form.Group>
            <Button block size="sg" type="submit">
              Create a New Project
            </Button>
          </Form>
        </div>
        <div className="rowElement1" />
        <div className="rowElement1" />
        <div className="rowElement1" />
      </div>
      <div className="row">
        <div className="rowElement1" />
        <div className="rowElement1" />
        <div className="rowElement1" />
        <div className="rowElement1" />
      </div>
      <div className="row">
        <div className="rowElement1" />
        <div className="rowElement1" />
        <div className="rowElement1" />
        <div className="rowElement1" />
      </div>
    </div>
  );
}

export default DashboardPage;
