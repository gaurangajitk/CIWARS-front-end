/**
 *
 * App
 *
 * This component is the skeleton around the actual pages, and should only
 * contain code that should be seen on all pages. (e.g. navigation bar)
 */

import React from 'react';
import { Helmet } from 'react-helmet';
import styled from 'styled-components';
import { Switch, Route } from 'react-router-dom';

import ReferPage from 'containers/ReferPage/Loadable';
import LoginPage from 'containers/LoginPage/Loadable';
import DashboardPage from 'containers/DashboardPage/Loadable';
import RegisterPage from 'containers/RegisterPage/Loadable';
import FeaturePage from 'containers/FeaturePage/Loadable';
import NotFoundPage from 'containers/NotFoundPage/Loadable';
import UserHomePage from 'containers/UserHomePage/Loadable';
import HomePage from 'containers/HomePage/Loadable';
import SubmitSamplePage from 'containers/SubmitSamplePage/Loadable';
import SubmitMultipleSamplesPage from 'containers/SubmitMultipleSamplesPage/Loadable';
import SubmitToolsPage from 'containers/SubmitToolsPage/Loadable';
import ShowErrorPage from 'containers/ShowErrorPage/Loadable';
import Header from 'components/Header';
import Footer from 'components/Footer';

import GlobalStyle from '../../global-styles';

const AppWrapper = styled.div`
  margin: 0 auto;
  display: flex;
  min-height: 100%;
  flex-direction: column;
`;

export default function App() {
  return (
    <AppWrapper>
      <Helmet titleTemplate="%s - CIWARS" defaultTitle="CIWARS">
        <meta name="description" content="CIWARS" />
      </Helmet>
      <Header />
      <Switch>
        <Route exact path="/" component={HomePage} />
        <Route path="/features" component={FeaturePage} />
        <Route path="/login" component={LoginPage} />
        <Route path="/submitSample" component={SubmitSamplePage} />
        <Route path="/userHomePage" component={UserHomePage} />
        <Route path="/submitMultipleSamples" component={SubmitMultipleSamplesPage} />
        <Route path="/submitTools" component={SubmitToolsPage} />
        <Route path="/register" component={RegisterPage} />
        <Route path="/dashboard" component={DashboardPage} />
        <Route path="/refer" component={ReferPage} />
        <Route path="/showError" component={ShowErrorPage}/>
        <Route path="" component={NotFoundPage} />
      </Switch>
      <GlobalStyle />
    </AppWrapper>
  );
}
