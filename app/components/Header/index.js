import React from 'react';
import { Nav, Navbar } from 'react-bootstrap';

function Header() {
  return (
    <Navbar expand="lg" bg="dark" variant="dark">
      <Navbar.Brand href="/">CIWARS</Navbar.Brand>
      <Navbar.Toggle aria-controls="responsive-navbar-nav" />
      <Navbar.Collapse id="responsive-navbar-nav">
        <Nav className="mr-auto">
          <Nav.Link href="/dashboard">Home</Nav.Link>
          <Nav.Link href="/submitSample">Submit Sample</Nav.Link>
          <Nav.Link href="/submitMultipleSamples">Submit Samples</Nav.Link>
          <Nav.Link href="/userHomePage">Uploaded Samples</Nav.Link>
          <Nav.Link href="/submitTools">Submit Tools</Nav.Link>
        </Nav>
        <Nav>
          <Nav.Link href="/login">Login</Nav.Link>
          <Nav.Link eventKey={2} href="/refer">
            Refer this
          </Nav.Link>
          <Nav.Link href="/features">Sample List</Nav.Link>
        </Nav>
      </Navbar.Collapse>
    </Navbar>
  );
}

export default Header;
