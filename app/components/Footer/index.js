import React from 'react';
import { FormattedMessage } from 'react-intl';

import Wrapper from './Wrapper';
import messages from './messages';

function Footer() {
  return (
    <Wrapper>
      <section />
      <section>
        <FormattedMessage {...messages.ciwarsMessage} />
      </section>
    </Wrapper>
  );
}

export default Footer;
