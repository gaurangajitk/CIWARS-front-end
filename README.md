CIWARS webservice

Steps to run :-


1. Install latest stable Node.js from https://nodejs.org/en/
2. Clone this repo using the following command:
    git clone --depth=1 https://code.vt.edu/gaurangajitk/CIWARS-front-end.git <YOUR_PROJECT_NAME>
3. Move to the appropriate directory: cd <YOUR_PROJECT_NAME>.
4. Run **npm run setup** in order to install dependencies and clean the git repo.
5. Run **npm install**
6. Run **npm start** to see the app at http://localhost:3000.
